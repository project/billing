<?php

namespace Drupal\billing\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Billing account entity.
 *
 * @ingroup billing
 *
 * @ContentEntityType(
 *   id = "billing_account",
 *   label = @Translation("Billing account"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\billing\Entity\BillingAccountListBuilder",
 *     "views_data" = "Drupal\billing\Entity\BillingAccountViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\billing\Form\BillingAccountForm",
 *       "add" = "Drupal\billing\Form\BillingAccountForm",
 *       "edit" = "Drupal\billing\Form\BillingAccountForm",
 *       "delete" = "Drupal\billing\Form\BillingAccountDeleteForm",
 *     },
 *     "access" = "Drupal\billing\Entity\BillingAccountAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\billing\Entity\BillingAccountHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "billing_account",
 *   admin_permission = "administer billing account entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/billing/billing_account/{billing_account}",
 *     "add-form" = "/billing/billing_account/add",
 *     "edit-form" = "/billing/billing_account/{billing_account}/edit",
 *     "delete-form" = "/billing/billing_account/{billing_account}/delete",
 *     "collection" = "/billing/billing_account",
 *   },
 *   field_ui_base_route = "billing_account.settings"
 * )
 */
class BillingAccount extends ContentEntityBase implements BillingAccountInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
    if (isset($values['entity_type']) && $values['entity_type'] == 'user') {
      $values += [
        'user_id' => $values['entity_id'],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);

    foreach ($entities as $entity) {
      $accounts = \Drupal::entityQuery('billing_transaction')
        ->condition('account_id', $entity->id->value)
        ->execute();
      foreach ($accounts as $id) {
        $transaction = BillingTransaction::load($id);
        try {
          $transaction->delete();
        }
        catch (\Exception $e) {
          \Drupal::messenger()->addWarning(t('Fail delete transaction @id', ['@id' => $id]));
        }
      }
    }
  }

  /**
   * Parent Entity.
   */
  public function getParentEntity() {
    $entity = FALSE;
    $entity_type = $this->get('entity_type')->value;
    $entity_id = $this->get('entity_id')->value;
    if ($entity_type && $entity_id && $entity_type != 'system') {
      $storage = \Drupal::service('entity_type.manager')->getStorage($entity_type);
      if (is_numeric($entity_id)) {
        $entity = [$entity_type => $storage->load($entity_id)];
      }
    }
    return $entity;
  }

  /**
   * Amount.
   */
  public function getAmount() {
    return (int) $this->get('amount')->value;
  }

  /**
   * Currency.
   */
  public function getCurrency() {
    return $this->get('currency')->target_id;
  }

  /**
   * Currency Default.
   */
  public static function getDefaultCurrency() {
    return 'XXX';
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Billing account entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Billing account entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('Account entity_type etc: user, system, node, order.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('user')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Entity ID'))
      ->setDescription(t('Entity id.'))
      ->setSettings([
        'min' => 0,
      ])
      ->setDefaultValue(0);

    $fields['currency'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Currency'))
      ->setSetting('target_type', 'currency')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\billing\Entity\BillingAccount::getDefaultCurrency')
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 5,
        'settings' => [
          'link' => FALSE,
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['amount'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Amount'))
      ->setDescription(t('Current credit.'))
      ->setSettings([
        'precision' => 19,
        'scale' => 6,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDefaultValue(0);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Billing account is published.'))
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
